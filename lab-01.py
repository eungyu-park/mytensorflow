##############GDRIVE SYNC TEST
# -*- coding: utf-8 -*- 
import tensorflow as tf

#1      ################################### 
node1 = tf.constant(3.0, tf.float32)
node2 = tf.constant(4.0)
node3 = tf.add(node1, node2)

print("node1: ", node1, "node2: ", node2)
print("node3: ", node3)

sess = tf.Session()
print(sess.run([node1, node2]))
print(sess.run(node3))
# TensorFlow는 그래프를 우선 만들어야 함.
# 그 다음, 그래프를 sess.run(op)로 실행해야 함.


#2     ################################### 
a = tf.placeholder(tf.float32)
b = tf.placeholder(tf.float32)

adder_node = a + b

print(sess.run(adder_node, feed_dict={a: 3, b: 4.5}))
print(sess.run(adder_node, feed_dict={a: [1, 3], b: [2, 4]}))
# TensorFlow에서 그래프를 미리(Placeholder) 만들어 놓을 수 있음.
# feed_dict로 데이터를 그래프로 넘길 수 있음.