
# -*- coding: utf-8 -*- 
# Lab 6 Softmax Classifier
import tensorflow as tf
import numpy as np
tf.set_random_seed(777)  # for reproducibility

# Predicting animal type based on various features
xy = np.loadtxt('data-04-zoo.csv', delimiter=',', dtype=np.float32)
x_data = xy[:, 0:-1]
y_data = xy[:, [-1]]

print(x_data.shape, y_data.shape)

# 분류는 총 7가지로, 즉 7개의 클래스를 가짐.
nb_classes = 7  # 0 ~ 6

# 16개의 특징을 한번에 입력으로 X에 넣음
# 16개의 특징에 대한 실제 결과값은 1개, 1개를 한번에 입력으로 Y에 넣음
X = tf.placeholder(tf.float32, [None, 16])
Y = tf.placeholder(tf.int32, [None, 1])  # 0 ~ 6

# TensorFlow의 one_hot을 사용하면, 차원이 한개 더 늘어남.
# 예제
# [[0][3][4]]가 one_hot만 사용함 => [[[1000000][0001000][0000100]]]
# 늘어난 차원을 다시 복구하여 사용하기 위해서 TensorFlow의 reshape를 사용함.
Y_one_hot = tf.one_hot(Y, nb_classes)  # one hot
print("one_hot", Y_one_hot)

# reshape 사용, Y_one_hot을 [-1, nb_classes]의 형태로 바꾸어 다시 Y_one_hot에 저장함.
# [[[1000000][0001000][0000100]]] => [[1000000][0001000][0000100]]
Y_one_hot = tf.reshape(Y_one_hot, [-1, nb_classes])
print("reshape", Y_one_hot)

# Weight 설정, 16개의 입력값에 각각 weight를 랜덤으로 할당, 출력 결과는 7가지의 경우의 수를 가짐.
# Bias 설정, 출력 결과는 7가지의 경우의 수를 가짐.
W = tf.Variable(tf.random_normal([16, nb_classes]), name='weight')
b = tf.Variable(tf.random_normal([nb_classes]), name='bias')

# tf.nn.softmax computes softmax activations
# softmax = exp(logits) / reduce_sum(exp(logits), dim)
# logits 설정
logits = tf.matmul(X, W) + b
hypothesis = tf.nn.softmax(logits)

# Cross entropy cost/loss
# logit을 softmax_cross_entropy_with_logits에 넣음.
# #50, #51, #52에 있는 코드는 #49의 코드와 동치.
# cost = tf.reduce_mean(-tf.reduce_sum(Y * tf.log(hypothesis), axis=1))
cost_i = tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits,
                                                 labels=tf.stop_gradient([Y_one_hot]))
cost = tf.reduce_mean(cost_i)
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1).minimize(cost)


# 예측 값과 실제 결과를 비교하여 학습 모델의 정확도를 보기 위해 correct_prediction, accuracy 변수를 설정.
# hypothesis의 확률 값을 0 ~ 6 중 한개의 값으로 변경
prediction = tf.argmax(hypothesis, 1)
# 위의 prediction과 Y_onehot에서 추정된 0 ~ 6 중 한개의 값과 비교, 결과를 저장.
correct_prediction = tf.equal(prediction, tf.argmax(Y_one_hot, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
# Launch graph
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    for step in range(2000):
        sess.run(optimizer, feed_dict={X: x_data, Y: y_data})
        if step % 100 == 0:
            loss, acc = sess.run([cost, accuracy], feed_dict={
                                 X: x_data, Y: y_data})
            print("Step: {:5}\tLoss: {:.3f}\tAcc: {:.2%}".format(
                step, loss, acc))

    # Let's see if we can predict
    pred = sess.run(prediction, feed_dict={X: x_data})

    # flatten 함수 기능
    # y_data: (N,1) = flatten => (N, ) matches pred.shape
    for p, y in zip(pred, y_data.flatten()):
        print("[{}] Prediction: {} True Y: {}".format(p == int(y), p, int(y)))

'''
Step:     0 Loss: 5.106 Acc: 37.62%
Step:   100 Loss: 0.800 Acc: 79.21%
Step:   200 Loss: 0.486 Acc: 88.12%
Step:   300 Loss: 0.349 Acc: 90.10%
Step:   400 Loss: 0.272 Acc: 94.06%
Step:   500 Loss: 0.222 Acc: 95.05%
Step:   600 Loss: 0.187 Acc: 97.03%
Step:   700 Loss: 0.161 Acc: 97.03%
Step:   800 Loss: 0.140 Acc: 97.03%
Step:   900 Loss: 0.124 Acc: 97.03%
Step:  1000 Loss: 0.111 Acc: 97.03%
Step:  1100 Loss: 0.101 Acc: 99.01%
Step:  1200 Loss: 0.092 Acc: 100.00%
Step:  1300 Loss: 0.084 Acc: 100.00%
...
[True] Prediction: 0 True Y: 0
[True] Prediction: 0 True Y: 0
[True] Prediction: 3 True Y: 3
[True] Prediction: 0 True Y: 0
[True] Prediction: 0 True Y: 0
[True] Prediction: 0 True Y: 0
[True] Prediction: 0 True Y: 0
[True] Prediction: 3 True Y: 3
[True] Prediction: 3 True Y: 3
[True] Prediction: 0 True Y: 0
'''
