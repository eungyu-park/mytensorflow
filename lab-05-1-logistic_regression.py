
# -*- coding: utf-8 -*- 
# Lab 5 Logistic Regression Classifier
# ~Lab 4까지는 Gradient Decent Algorithm을 사용: 
#   어떤 점에서의 기울기를 기준으로 타겟을 찾음.
#   한번에 움직이는 단위는 Stepsize(= Learning Rate)
#   

# Classification
# 이전에 했던 것은 숫자를 예측, 이번에 하는 것은 Binary Classification
# 주어진 입력을 2가지로 분류.
# 예시로는 메일의 스팸/정상, 페이스북의 숨기기/보이기, FDS의 허가/거부
# 주로 0 혹은 1로 태그.

# 단점: 지나치게 높은 인풋이 존재할 경우, 전체적인 커트라인?이 쓸데없이 올라가서
# 실제로는 합격이지만 불합격으로 예상되는 경우가 생기기 시작함.

# 어떠한 입력이 주어지더라도 (0, 1)의 범위에 만족하는 함수를 찾음
# => sigmoid(= Logistic Function)
# 입력 z가 커진다면, g(z)는 1에 가까워짐. 입력 z가 작아지면, g(z)는 0에 가까워짐
# 따라서, z = Wx, H(x) = g(z)로 가정.
# 즉, H(x) = g(Wx) = 1/[1 + e^{W^(T) * -X}]

# Gradient는 곡선이 매끄러움 => 뭘 하든 어떻게든 최소로 가게 됨.
# Classification => 시작점에 따라 최소를 가지는 지점이 달라질 수 있음.
# 따라서, 전체적인 최소(Global Minimal)을 찾기 위해 Cost 함수도 바꿔야 함.

# e의 구부러짐을 잡기 위해서 log를 사용

import tensorflow as tf
tf.set_random_seed(777)  # for reproducibility

x_data = [[1, 2],
          [2, 3],
          [3, 1],
          [4, 3],
          [5, 3],
          [6, 2]]
y_data = [[0],
          [0],
          [0],
          [1],
          [1],
          [1]]

# placeholders for a tensor that will be always fed.
X = tf.placeholder(tf.float32, shape=[None, 2])
Y = tf.placeholder(tf.float32, shape=[None, 1])

# x는 2개의 요소로 들어옴, y는 1개의 요소로 들어옴. 따라서 W는 [2,1]
W = tf.Variable(tf.random_normal([2, 1]), name='weight')
b = tf.Variable(tf.random_normal([1]), name='bias')

# Hypothesis using sigmoid: tf.div(1., 1. + tf.exp(tf.matmul(X, W)))
# sigmoid 함수에 인자로 넣음.
hypothesis = tf.sigmoid(tf.matmul(X, W) + b)

# cost/loss function
# cost 정의.
cost = -tf.reduce_mean(Y * tf.log(hypothesis) + (1 - Y) *
                       tf.log(1 - hypothesis))

# GradientDescentOptimizer: 자동으로 주어진 설정으로 학습 모델 제시
# 여기서는 learning rate와 미분 제공.
train = tf.train.GradientDescentOptimizer(learning_rate=0.01).minimize(cost)

# Accuracy computation
# True if hypothesis>0.5 else False
# 0.5 이상일 시, True 혹은 False. float32로 dtype을 정의하면 True가 1.
predicted = tf.cast(hypothesis > 0.5, dtype=tf.float32)

# 예측에 대한 정확도 계산
accuracy = tf.reduce_mean(tf.cast(tf.equal(predicted, Y), dtype=tf.float32))

# Launch graph
with tf.Session() as sess:
    # Initialize TensorFlow variables
    sess.run(tf.global_variables_initializer())

    for step in range(10001):
        cost_val, _ = sess.run([cost, train], feed_dict={X: x_data, Y: y_data})
        if step % 50 == 0:
            print(step, cost_val)

    # Accuracy report
    h, c, a = sess.run([hypothesis, predicted, accuracy],
                       feed_dict={X: x_data, Y: y_data})
    print("\nHypothesis: ", h, "\nCorrect (Y): ", c, "\nAccuracy: ", a)

'''
0 1.73078
200 0.571512
400 0.507414
600 0.471824
800 0.447585
...
9200 0.159066
9400 0.15656
9600 0.154132
9800 0.151778
10000 0.149496

Hypothesis:  [[ 0.03074029]
 [ 0.15884677]
 [ 0.30486736]
 [ 0.78138196]
 [ 0.93957496]
 [ 0.98016882]]
Correct (Y):  [[ 0.]
 [ 0.]
 [ 0.]
 [ 1.]
 [ 1.]
 [ 1.]]
Accuracy:  1.0
'''
