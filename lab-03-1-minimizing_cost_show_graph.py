
# -*- coding: utf-8 -*- 
# #2018.10.02에 시작.

# Cost가 가장 작은 W값을 찾아야 함.
# 기계적으로 접근해야 하기에, Gradient descent algoritm이 사용됨.
# 주어진 Cost Function을 minimize하는 데 사용됨.
# w1, w2, w3, ..., b1, b2, b3, ...을 추정하는 데 사용됨.

# Gradient descent algoritm
# 기울기를 특정 점에서 계속 측정
# 기울기가 0에 가장 가까울 때까지 계속 점의 위치(W, b)를 옮김.
# 어느 점에서 시작하던지, 최종점은 항상 똑같음.
# 항상 답을 찾을 수 있음.
# 기울기는 미분을 이용.

# Let cost(W) = 1/m * SUM(i=1, i->m){(W * x^i - y^i)^2}
# 계산의 편의를 위해, 1/2를 곱.
# cost(W) = 1/(2 * m) * SUM(i=1, i->m){(W * x^i - y^i)^2}

# W에 관하여 미분.
# W - alpha(Lerning rate, 약 0.1) * {(d/dW) *cost(W)}
# W := W - alpha * 1/m * SUM(i=1, i->m){(W * x^i - y^i)^2) * x^i}


# Lab 3 Minimizing Cost
import tensorflow as tf
import matplotlib.pyplot as plt
tf.set_random_seed(777)  # for reproducibility

X = [1, 2, 3]
Y = [1, 2, 3]

# 간략화를 위해 변수를 W 하나로만 함.
W = tf.placeholder(tf.float32)

# Our hypothesis for linear model X * W
hypothesis = W * X

# cost/loss function
cost = tf.reduce_mean(tf.square(hypothesis - Y))

# Launch the graph in a session.
sess = tf.Session()

# W와 cost를 저장할 리스트 정의.
W_history = []
cost_history = []

# W의 범위 지정, -3 ~ +5단위로 
for i in range(-30, 50):
    curr_W = i * 0.1
    curr_cost = sess.run(cost, feed_dict={W: curr_W})
    W_history.append(curr_W)
    cost_history.append(curr_cost)

# Show the cost function
plt.plot(W_history, cost_history)
plt.show()
