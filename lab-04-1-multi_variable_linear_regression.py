
# -*- coding: utf-8 -*- 
# Lab 4 Multi-variable linear regression

# 원소가 여러개가 될 때도 일반적인 다항식을 쓸 수는 없음.
# 따라서, 행렬을 이용함.
# 행렬에서 입력으로 주어지는 행렬, 즉 x로 구성된 행렬의 한 행을 Instance라고 함.
# 인스턴스가 여러 개가 한 세트로 데이터가 주어짐.
# 이 경우에, 인스턴스 한 개마다 weight를 곱하게 됨.
# 인스턴스를 사용할 때, 행렬 1개를 넣으면 전체 결과가 한꺼번에 나온다는 장점을 가짐.
# [5, 3]: 5개의 Data Samples 즉, Instances. 3개의 입력 변수.
# [3, 1]: 3개의 Weights. 1종류의 Weight.
# 따라서, [5, 1]의 행렬이 출력됨. -> 1종류의 Y(결과)

# 우리가 결정해야 할 것은 Weight의 크기이다.
# Weight은 일단 [변수의 개수, 결과의 종류]이다.
# Instance의 개수와 Weights의 개수는 동일함.

# 통상적으로, [n, 3], [3, 1], [n, 1]의 형태로 표기됨.
# N: None, 즉 아무거나 가능.

# Instance를 사용하면 출력이 여러 개라도, Data Set이 여러 개여도 쉽게 처리가 가능.

# 주의 사항
#   이론적으로는 H(x) = Wx + b 의 형태.
#   TensorFlow로 구현을 할 때에는 H(x) = XW 의 형태.(단, X, W는 둘 다 행렬)
#       왜냐하면, 한 번에 처리가 가능함.

# 4-1 시작
# Instance가 늘어날 수록, 예측이 정확해질 확률이 커짐.
# 


import tensorflow as tf
tf.set_random_seed(777)  # for reproducibility

x1_data = [73., 93., 89., 96., 73.]
x2_data = [80., 88., 91., 98., 66.]
x3_data = [75., 93., 90., 100., 70.]

y_data = [152., 185., 180., 196., 142.]

# placeholders for a tensor that will be always fed.
x1 = tf.placeholder(tf.float32)
x2 = tf.placeholder(tf.float32)
x3 = tf.placeholder(tf.float32)

Y = tf.placeholder(tf.float32)

w1 = tf.Variable(tf.random_normal([1]), name='weight1')
w2 = tf.Variable(tf.random_normal([1]), name='weight2')
w3 = tf.Variable(tf.random_normal([1]), name='weight3')
b = tf.Variable(tf.random_normal([1]), name='bias')

hypothesis = x1 * w1 + x2 * w2 + x3 * w3 + b

# cost/loss function
cost = tf.reduce_mean(tf.square(hypothesis - Y))

# Minimize. Need a very small learning rate for this data set
optimizer = tf.train.GradientDescentOptimizer(learning_rate=1e-5)
train = optimizer.minimize(cost)

# Launch the graph in a session.
sess = tf.Session()
# Initializes global variables in the graph.
sess.run(tf.global_variables_initializer())

# 다음 구문의 의미
# step은 1씩 증가, step이 0부터 시작하고, 2000이 될 때까지 반복
#   sess.run에 [cost, hypothesis, train] 배열을 인자로 줌
#       sess.run으로 나온 3가지 결과를 cost_val, hy_bal, _에 저장함.
#       feed_dict를 통해, placeholder x1, x2, x3, Y에 개별적으로 x1_data, x2_data, x3_data, y_data를 줌.
#       feed_dict은 인자로 주어진 것들을 sess.run을 할 수 있게 넘겨줌.
#   step이 10으로 나눠질 때마다 현재 상태를 출력함.
for step in range(100000):
    cost_val, hy_val, _ = sess.run([cost, hypothesis, train],
                                   feed_dict={x1: x1_data, x2: x2_data, x3: x3_data, Y: y_data})
    if step % 1 == 0:
        print(step, "Cost: ", cost_val, "\nPrediction:\n", hy_val)

# 위 방식은 잘 쓰이지 않음. 

'''
0 Cost:  19614.8
Prediction:
 [ 21.69748688  39.10213089  31.82624626  35.14236832  32.55316544]
10 Cost:  14.0682
Prediction:
 [ 145.56100464  187.94958496  178.50236511  194.86721802  146.08096313]

 ...

1990 Cost:  4.9197
Prediction:
 [ 148.15084839  186.88632202  179.6293335   195.81796265  144.46044922]
2000 Cost:  4.89449
Prediction:
 [ 148.15931702  186.8805542   179.63194275  195.81971741  144.45298767]
'''
