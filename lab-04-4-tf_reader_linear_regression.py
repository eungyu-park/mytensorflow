
# -*- coding: utf-8 -*- 
# Lab 4 Multi-variable linear regression
# https://www.tensorflow.org/programmers_guide/reading_data

# numpy를 이용하여 파일을 읽어오는 것은 가능
# 그리나, 메모리를 과도하게 요구할 수 있기에 Queue Runner라는 것을 이용함.
# Queue Runner 사용법
import tensorflow as tf
tf.set_random_seed(777)  # for reproducibility

# 1: 파일 이름으로 된 리스트를 제작.
filename_queue = tf.train.string_input_producer(
    ['data-01-test-score.csv'], shuffle=False, name='filename_queue')

# 2: 파일 이름으로 구성된 큐의 한 요소에서 데이터를 읽어올 리더를 정의
# 2: Text를 읽는다.
reader = tf.TextLineReader()

# 2: key, value를 기준으로, filename_queue에서 읽는다.
key, value = reader.read(filename_queue)

# Default values, in case of empty columns. Also specifies the type of the
# decoded result.
# 3: tf.decode_csv의 value는 리더를 통해 읽어온 value
# 3: value를 어떻게 파싱할 것인가? => decode_csv 내부의 정보로 파싱.
# 3: 여기서는 record_defaults의 형태, 즉 [[0.], [0.], [0.], [0.]]
# 의 형태(float 데이터 형식)로 읽음. 만약 NULL이라면, 0을 배치.
# 3: csv의 형태를 가져와 decode.
record_defaults = [[0.], [0.], [0.], [0.]]
xy = tf.decode_csv(value, record_defaults=record_defaults)

# collect batches of csv in
# tf.train.batch: 데이터를 읽어올 수 있게 하는 펌프 역할
# train_x_batch에는 xy[0:-1]를,
# train_y_batch에는 xy[-1:]를,  10개씩 가져와 넣음.
train_x_batch, train_y_batch = \
    tf.train.batch([xy[0:-1], xy[-1:]], batch_size=10)

# placeholders for a tensor that will be always fed.
# X의 데이터 묶음 1개에는 3개의 요소, Y는 1개.
X = tf.placeholder(tf.float32, shape=[None, 3])
Y = tf.placeholder(tf.float32, shape=[None, 1])

# Wx이므로, W에는 1개의 x가 할당되는데, 1개의 x에는 3개의 요소가 존재.
# 즉, [3, 1]
W = tf.Variable(tf.random_normal([3, 1]), name='weight')
b = tf.Variable(tf.random_normal([1]), name='bias')

# Hypothesis
hypothesis = tf.matmul(X, W) + b

# Simplified cost/loss function
cost = tf.reduce_mean(tf.square(hypothesis - Y))

# Minimize
optimizer = tf.train.GradientDescentOptimizer(learning_rate=1e-5)
train = optimizer.minimize(cost)

# Launch the graph in a session.
sess = tf.Session()
# Initializes global variables in the graph.
sess.run(tf.global_variables_initializer())

# Start populating the filename queue.
# 다음 두 줄은, 파일을 입력으로 가져올 시, 일반적으로 쓰이는 구문.
coord = tf.train.Coordinator()
threads = tf.train.start_queue_runners(sess=sess, coord=coord)

for step in range(2001):
    # train_x_batch와 y_train_batch로부터 데이터를 가져와
    # x_batch, y_batch에 넣음.
    x_batch, y_batch = sess.run([train_x_batch, train_y_batch])
    
    cost_val, hy_val, _ = sess.run(
        [cost, hypothesis, train], 
        feed_dict={X: x_batch, Y: y_batch})
    if step % 10 == 0:
        print(step, "Cost: ", cost_val, "\nPrediction:\n", hy_val)

# 다음 두 줄은, 파일을 입력으로 가져올 시, 일반적으로 쓰이는 구문.
coord.request_stop()
coord.join(threads)

# Ask my score
print("Your score will be ",
      sess.run(hypothesis, feed_dict={X: [[100, 70, 101]]}))

print("Other scores will be ",
      sess.run(hypothesis, feed_dict={X: [[60, 70, 110], [90, 100, 80]]}))

'''
Your score will be  [[ 177.78144836]]
Other scores will be  [[ 141.10997009]
 [ 191.17378235]]

'''
