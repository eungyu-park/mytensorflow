
# -*- coding: utf-8 -*- 
# Lab 6 Softmax Classifier
# Binary Classification을 통해 Multinomial Classification을 구현 가능
# 여러 Binary Classfication을 디자인, 3개의 구분은 3개의 Binary Classification 이용.

# Softmax
# 1. 최종 값이 [0, 1]에 부합함.
# 2. 최종 값들의 총 합은 1, 즉 확률로 취급이 가능
# 반환: Y's Hat(추정 Y값)

# argmax(ONE-HOT Encoding): 최종 값들 중 1개만을 1.0으로 지정, 나머지는 0으로.
# 반환: L

# Cross Entropy(Cost Function으로 사용): Y's Hat과 L을 입력으로 받음
# =>> 예측 값이 실제와 같으면 cost를 감소, 다르면 급증.(-log 사용)

# Logistic cost vs Cross Entropy
# C:(H(x), y) = ylog(H(x)) - (1 - y)log(1 - H(x))
# => 실제 y값은 L로, H(x)는 S로.
# D:(S, L) = Sum[~ i](L_i * log(S_i))

# 최종적으로는 Gradient Decent Algorithm을 사용.
# 입력 그래프가 Cross Entropy를 사용하면 매끈하게 나옴.

import tensorflow as tf
tf.set_random_seed(777)  # for reproducibility

# 실제 입력값
x_data = [[1, 2, 1, 1],
          [2, 1, 3, 2],
          [3, 1, 3, 4],
          [4, 1, 5, 5],
          [1, 7, 5, 5],
          [1, 2, 5, 6],
          [1, 6, 6, 6],
          [1, 7, 7, 7]]

# 실제 출력값(One-Hot Encoding: 3가지 요소 중, 1가지 요소에만 1을 할당)
y_data = [[0, 0, 1],        #=2
          [0, 0, 1],        #=2 
          [0, 0, 1],        #=2
          [0, 1, 0],        #=1
          [0, 1, 0],        #=1
          [0, 1, 0],        #=1
          [1, 0, 0],        #=0
          [1, 0, 0]]        #=0

# Shape
# X: 무한개, 4개의 요소로 구성됨.
# Y: 무한개, 3개의 요소로 구성됨.(Lable의 개수, 즉 출력 클래스의 개수)
X = tf.placeholder("float", [None, 4])
Y = tf.placeholder("float", [None, 3])
nb_classes = 3

# X는 4개의 요소로, Y는 3개의 요소로 구성됨. 
W = tf.Variable(tf.random_normal([4, nb_classes]), name='weight')
b = tf.Variable(tf.random_normal([nb_classes]), name='bias')

# tf.nn.softmax computes softmax activations
# softmax = exp(logits) / reduce_sum(exp(logits), dim)
# Softmax 함수는 TensorFlow에서 제공.
hypothesis = tf.nn.softmax(tf.matmul(X, W) + b)
# 결과는 입력에 대한 예상 결과의 확률로 제공됨.

# Cross entropy cost/loss
cost = tf.reduce_mean(-tf.reduce_sum(Y * tf.log(hypothesis), axis=1))

# 알아서 미분, Learning rate, 최소를 해줌.
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1).minimize(cost)

# Launch graph
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    for step in range(100001):
        sess.run(optimizer, feed_dict={X: x_data, Y: y_data})
        if step % 100 == 0:
            print(step, sess.run(cost, feed_dict={X: x_data, Y: y_data}))

    print('--------------')

    # Testing & One-hot encoding
    # 실제 데이터와 실제 결과로 학습을 마친 후.
    # 임의의 입력을 제공하여 그동안 학습한 모델을 통해 출력을 예상하도록 함.
    # 여기서의 입력은 [1, 11, 7, 9], [1, 3, 4, 3], [1, 1, 0, 1].
    a = sess.run(hypothesis, feed_dict={X: [[1, 11, 7, 9]]})
    print(a, sess.run(tf.argmax(a, 1)))

    print('--------------')

    b = sess.run(hypothesis, feed_dict={X: [[1, 3, 4, 3]]})
    print(b, sess.run(tf.argmax(b, 1)))

    print('--------------')

    c = sess.run(hypothesis, feed_dict={X: [[1, 1, 0, 1]]})
    print(c, sess.run(tf.argmax(c, 1)))

    print('--------------')

    all = sess.run(hypothesis, feed_dict={
                   X: [[1, 11, 7, 9], [1, 3, 4, 3], [1, 1, 0, 1]]})

    #argmax: 확률이 가장 큰 것을 선택.
    print(all, sess.run(tf.argmax(all, 1)))

'''
--------------
[[  1.38904958e-03   9.98601854e-01   9.06129117e-06]] [1]
--------------
[[ 0.93119204  0.06290206  0.0059059 ]] [0]
--------------
[[  1.27327668e-08   3.34112905e-04   9.99665856e-01]] [2]
--------------
[[  1.38904958e-03   9.98601854e-01   9.06129117e-06]
 [  9.31192040e-01   6.29020557e-02   5.90589503e-03]
 [  1.27327668e-08   3.34112905e-04   9.99665856e-01]] [1 0 2]
'''
