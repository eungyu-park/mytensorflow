
# -*- coding: utf-8 -*- 
# Lab 4 Multi-variable linear regression
import tensorflow as tf

# 파일 열기 관련 라이브러리, numpy
import numpy as np
tf.set_random_seed(777)  # for reproducibility

# numpy로 파일 열기, 파일 이름으로부터 ','를 기준으로, float32 데이터 타입으로 텍스트를 읽음.
xy = np.loadtxt('data-01-test-score.csv', delimiter=',', dtype=np.float32)

# Python의 Slicing 기능
"""
예제: nums = range(5), 즉 [0, 1, 2, 3, 4]일때
print nums      => [0, 1, 2, 3, 4]
print nums[2:4] => [2, 3]
print nums[2:]  => [2, 3, 4]
print nums[:]   => [0, 1, 2, 3, 4]
print nums[:-1] => [0, 1, 2, 3]
"""

# x_data는 앞 3, y_data는 뒤 1
x_data = xy[:, 0:-1]
y_data = xy[:, [-1]]

# Make sure the shape and data are OK
print(x_data.shape, x_data, len(x_data))
print(y_data.shape, y_data)

# 데이터 개수에 따라 배열 크기 지정.
# placeholders for a tensor that will be always fed.
X = tf.placeholder(tf.float32, shape=[None, 3])
Y = tf.placeholder(tf.float32, shape=[None, 1])

# W에 주어지는 X의 개수가 3개이므로, 3. Wx는 1개로 나가므로 1.
W = tf.Variable(tf.random_normal([3, 1]), name='weight')
b = tf.Variable(tf.random_normal([1]), name='bias')

# 행렬 X와 행렬 W를 곱, 결과에 b를 더함.
# Hypothesis
hypothesis = tf.matmul(X, W) + b

# Simplified cost/loss function
cost = tf.reduce_mean(tf.square(hypothesis - Y))

# Minimize
optimizer = tf.train.GradientDescentOptimizer(learning_rate=1e-5)
train = optimizer.minimize(cost)

# Launch the graph in a session.
sess = tf.Session()
# Initializes global variables in the graph.
sess.run(tf.global_variables_initializer())

for step in range(2001):
    cost_val, hy_val, _ = sess.run(
        [cost, hypothesis, train], feed_dict={X: x_data, Y: y_data})
    if step % 10 == 0:
        print(step, "Cost: ", cost_val, "\nPrediction:\n", hy_val)

# Ask my score
print("Your score will be ", sess.run(
    hypothesis, feed_dict={X: [[100, 70, 101]]}))

print("Other scores will be ", sess.run(hypothesis,
                                        feed_dict={X: [[60, 70, 110], [90, 100, 80]]}))

'''
Your score will be  [[ 181.73277283]]
Other scores will be  [[ 145.86265564]
 [ 187.23129272]]

'''
