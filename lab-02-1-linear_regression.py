# Lab02: Linear Regression
# -*- coding: utf-8 -*- 

# Cost Function(Lost Function): 가설과 실제 데이터의 차이를 나타내는 함수
# 예를 들자면, (H(x) - y))^2 가 Cost라고 할 수 있음. (단 H(x) = Wx + b)
# W = Weight, b = bias

# 데이터의 개수를 m, H(x)를 가설(예측 값), y가 실제 데이터값이라 가정
# ->(H(x) - y)^2을 모두 더한(m번 더한) 다음, m으로 나눈 것이 cost(차이의 제곱의 평균값).
# cost는 실질적으로 cost(W, b)가 되게 됨. 학습이란 것은 cost를 가장 작게 만드는 W와 b의 값을 구하는 것.

# TensorFlow의 그래프를 디자인 -> sess.run을 통해서 그래프를 실행 -> 실행 결과를 업데이트하거나 반환값 체크

# Lab 2 Linear Regression
import tensorflow as tf
tf.set_random_seed(777)  # for reproducibility

# X and Y data
x_train = [1, 2, 3]
y_train = [1, 2, 3]

# Try to find values for W and b to compute y_data = x_data * W + b
# We know that W should be 1 and b should be 0
# Variable: 기존 프로그래밍에서의 변수와는 조금 다른 느낌. 내가 사용하는 변수가 아니라 TensorFlow가 알아서 변경하고 대입하는 변수.
# Variable을 정의할 때에는 Shape를 정의하고, 값을 정의함.
W = tf.Variable(tf.random_normal([1]), name='weight')
b = tf.Variable(tf.random_normal([1]), name='bias')

# Let H(x) = Wx + b
hypothesis = x_train * W + b

# Cost 정의, reduce_mean은 평균을 내주는 역할.
cost = tf.reduce_mean(tf.square(hypothesis - y_train))

# optimizer 정의, 무엇을 optimize할 것인지 대상 지정(cost)
# 이럴 경우 알아서 Variable(W, b)을 조정해서 학습함.
# train이라는 그래프의 이름을 정의.
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.01)
train = optimizer.minimize(cost)

# Session 정의
# Variable을 사용하기 위해서는 반드시 초기화를 해야 함.
sess = tf.Session()
sess.run(tf.global_variables_initializer())

# 2001번 실행.
for step in range(2001):
    sess.run(train)
    if step % 20 == 0:
        print(step, sess.run(cost), sess.run(W), sess.run(b))

# Learns best fit W:[ 1.],  b:[ 0.]

'''
0 2.82329 [ 2.12867713] [-0.85235667]
20 0.190351 [ 1.53392804] [-1.05059612]
40 0.151357 [ 1.45725465] [-1.02391243]
...

1920 1.77484e-05 [ 1.00489295] [-0.01112291]
1940 1.61197e-05 [ 1.00466311] [-0.01060018]
1960 1.46397e-05 [ 1.004444] [-0.01010205]
1980 1.32962e-05 [ 1.00423515] [-0.00962736]
2000 1.20761e-05 [ 1.00403607] [-0.00917497]
'''
